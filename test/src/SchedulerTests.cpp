/**
 * @file SchedulerTests.cpp
 *
 * This module contains the unit tests of the Timekeeping::Scheduler class.
 *
 */

#include <future>
#include <gtest/gtest.h>
#include <TimeKeeping/Clock.hpp>
#include <TimeKeeping/Scheduler.hpp>

namespace {

    struct MockClock 
        : public Timekeeping::Clock 
    {
        double currentTime = 0.0;

        virtual double GetCurrentTime() override {
            return currentTime;
        }
    };

}

struct SchedulerTests
    : public ::testing::Test
{

    Timekeeping::Scheduler scheduler;

    std::shared_ptr< MockClock > mockClock = std::make_shared< MockClock >();

    void AdvanceMockClock(double deltaTime) {
        mockClock->currentTime += deltaTime;
        scheduler.WakeUp();
    }

    virtual void SetUp() override {
        scheduler.SetClock(mockClock);
    }

    virtual void TearDown() override {

    }

};

TEST_F(SchedulerTests, Schedule) {
    std::promise< void > calledBack;
    const auto callback = [&calledBack]{ calledBack.set_value(); };
    auto calledBackFuture = calledBack.get_future();

    (void)scheduler.Schedule(callback, 10.0);
    AdvanceMockClock(5.0);
    const auto wasCalledEarly = (
        calledBackFuture.wait_for(std::chrono::milliseconds(100)) == std::future_status::ready
    );

    AdvanceMockClock(5.001);
    const auto wasCalledOnTime = (
        calledBackFuture.wait_for(std::chrono::milliseconds(100)) == std::future_status::ready
    );

    EXPECT_FALSE(wasCalledEarly);
    EXPECT_TRUE(wasCalledOnTime);
    
}

TEST_F(SchedulerTests, Cancel) {
    std::promise< void > calledBack;
    const auto callback = [&calledBack] { calledBack.set_value(); };
    auto calledBackFuture = calledBack.get_future();
    const auto token = scheduler.Schedule(callback, 10.0);

    AdvanceMockClock(5.0);
    scheduler.Cancel(token);
    AdvanceMockClock(5.001);
    const auto wasCalledOnTime = (
        calledBackFuture.wait_for(std::chrono::seconds(0))
        == std::future_status::ready
    );

    EXPECT_FALSE(wasCalledOnTime);
}

// 测试不 set up Scheduler 的 clock
TEST_F(SchedulerTests, ScheduleWithoutClock) {

    scheduler = Timekeeping::Scheduler();
    std::promise< void > calledBack;
    const auto callback = [&calledBack]{ calledBack.set_value(); };
    auto calledBackFuture = calledBack.get_future();


    const auto token = scheduler.Schedule(callback, 10.0);
    AdvanceMockClock(10.001);
    const auto wasCalledOnTime = (
        calledBackFuture.wait_for(std::chrono::seconds(0))
        == std::future_status::ready
    );


    EXPECT_EQ(0, token);
    EXPECT_FALSE(wasCalledOnTime);
}

TEST_F(SchedulerTests, GetClock) {
    // 测试设置的 clock
    scheduler = Timekeeping::Scheduler();
    scheduler.SetClock(mockClock);

    const auto clock = scheduler.GetClock();

    EXPECT_EQ(mockClock, clock);
}
