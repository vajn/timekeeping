#pragma once

/**
 * @file Clock.hpp
 *
 * This module declares the Timekeeping::Clock interface.
 *
 */

namespace Timekeeping {

    class Clock
    {
    public:

        /**
         *  This method returns the current time, in seconds, since the 
         *  clock's reference point
         */
        virtual double GetCurrentTime() = 0;
    };

}