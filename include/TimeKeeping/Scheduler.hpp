#pragma once

/**
 * @file Scheduler.hpp
 *
 * This module declares the Timekeeping::Scheduler class.
 * 
 */

#include "Clock.hpp"

#include <functional>
#include <memory>

namespace Timekeeping {

    class Scheduler {
    public:
        /**
         * This is the type of function which can be scheduled to be called
         *  by the scheduler
         */
        using Callback = std::function< void() >;

    public:
        ~Scheduler() noexcept;
        Scheduler(const Scheduler&) = delete;
        Scheduler(Scheduler&&) noexcept;
        Scheduler& operator=(const Scheduler&) = delete;
        Scheduler& operator=(Scheduler&&) noexcept;

    public:

        /**
         * Return the clock object used to know when to call scheduled
         *  callbacks.
         */
        Scheduler();

        /**
         * Return the clock object used to know when to call scheduled
         * callbacks
         * 
         */
        std::shared_ptr< Clock > GetClock() const;

        /**
         * Set the clock object used to know when to call scheduled callback
         */
        void SetClock(std::shared_ptr< Clock > clock);

        /**
         *  Schedule the given callback function to be called when the
         *  time returned by the clock associated with rhe scheduler
         *  reaches the given due time
         */
        int Schedule(
            Callback callback,
            double due
        );

        /**
         *  Terminate the scheduled callback corresponding to the given token
         */
        void Cancel(int token);

        void WakeUp();

    private:

        struct Impl;

        std::unique_ptr< Impl > impl_;

    };

}