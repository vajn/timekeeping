# Timekeeping
用于定时调度执行回调函数

```

TEST_F(SchedulerTests, Schedule) {
    std::promise< void > calledBack;
    const auto callback = [&calledBack]{ calledBack.set_value(); };
    auto calledBackFuture = calledBack.get_future();

    (void)scheduler.Schedule(callback, 10.0);
    AdvanceMockClock(5.0);
    const auto wasCalledEarly = (
        calledBackFuture.wait_for(std::chrono::milliseconds(100)) == std::future_status::ready
    );

    AdvanceMockClock(5.001);
    const auto wasCalledOnTime = (
        calledBackFuture.wait_for(std::chrono::milliseconds(100)) == std::future_status::ready
    );

    EXPECT_FALSE(wasCalledEarly);
    EXPECT_TRUE(wasCalledOnTime);
    
}

```
